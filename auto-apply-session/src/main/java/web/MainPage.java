package web;

import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Named
@RequestScoped
public class MainPage {
    HttpServletRequest req = Faces.getRequest();

    Principal userPrincipal;

    @PostConstruct
    public void init() {
        userPrincipal = req.getUserPrincipal();
    }

    public Principal getUserPrincipal() {
        return userPrincipal;
    }

    public String logout() throws ServletException {
        req.logout();
        Faces.getSession().invalidate();
        return Faces.getViewId() + "?faces-redirect=true";
    }
}
