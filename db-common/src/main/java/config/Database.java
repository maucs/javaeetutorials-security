package config;

import javax.annotation.sql.DataSourceDefinition;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
public class Database {
}
