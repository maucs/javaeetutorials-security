package service;


import entity.ApplicationUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserService {
    @PersistenceContext
    EntityManager em;

    public void add(ApplicationUser u) {
        em.persist(u);
    }
}
