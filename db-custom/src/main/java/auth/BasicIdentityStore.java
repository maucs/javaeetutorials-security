package auth;

import entity.ApplicationUser;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

@Stateless
public class BasicIdentityStore implements IdentityStore {
    @PersistenceContext
    EntityManager em;

    @Inject
    CustomHash hash;

    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        try {
            ApplicationUser u = em.createQuery("SELECT u FROM ApplicationUser u WHERE u.username = :name", ApplicationUser.class)
                    .setParameter("name", credential.getCaller())
                    .getSingleResult();

            if (hash.verify(credential.getPassword().getValue(), u.getPassword()))
                return new CredentialValidationResult(u.getUsername());
        } catch (NoResultException e) {
        }
        return CredentialValidationResult.INVALID_RESULT;
    }
}
