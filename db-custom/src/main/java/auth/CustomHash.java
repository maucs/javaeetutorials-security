package auth;

import javax.security.enterprise.identitystore.PasswordHash;
import java.util.Map;
import java.util.Random;

/**
 * Very simple and faulty hasher that capitalizes all password, adds a random faulty salt, and appends it
 * <p>
 * Salt should be derived from {@link java.security.SecureRandom}
 * Use proper hash functions, like Pbkdf2
 */
public class CustomHash implements PasswordHash {
    private static final String MULTIPLIER = "CustomHash.Multiplier";
    private int multiplier;

    @Override
    public String generate(char[] password) {
        int salt = new Random().nextInt(100);
        return multiplier + ":" + salt + ":" + generate(multiplier, salt, password);
    }

    /**
     * A stateless way of generating only the hash
     *
     * @param multiplier
     * @param salt
     * @param password
     * @return The hash, without multiplier or salt appended to it
     */
    private String generate(int multiplier, int salt, char[] password) {
        return new String(password).toUpperCase() + (salt * multiplier);
    }

    @Override
    public boolean verify(char[] password, String hashedPassword) {
        String[] tokens = hashedPassword.split(":");
        try {
            return generate(parse(tokens[0]), parse(tokens[1]), password).equals(tokens[2]);
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public void initialize(Map<String, String> parameters) {
        for (Map.Entry<String, String> entry : parameters.entrySet())
            switch (entry.getKey()) {
                case MULTIPLIER:
                    try {
                        multiplier = Integer.parseInt(entry.getValue());
                    } catch (NumberFormatException e) {
                        // ignore
                    }
                    break;
            }
    }

    /**
     * Helper for Integer.parse
     *
     * @param v
     * @return integer
     */
    private int parse(String v) {
        return Integer.parseInt(v);
    }
}
