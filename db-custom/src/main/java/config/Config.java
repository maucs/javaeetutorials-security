package config;

import auth.CustomHash;
import entity.ApplicationUser;
import service.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Singleton
@Startup
public class Config {
    @EJB
    UserService service;

    @Inject
    CustomHash hash;

    @PostConstruct
    public void init() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("CustomHash.Multiplier", "20");
        hash.initialize(parameters);

        service.add(new ApplicationUser("bob", hash.generate("builder".toCharArray())));
        service.add(new ApplicationUser("cat", hash.generate("builder".toCharArray())));
    }
}
