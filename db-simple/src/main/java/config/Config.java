package config;

import entity.ApplicationUser;
import service.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.IdentityStore;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;
import java.util.HashMap;
import java.util.Map;

@DatabaseIdentityStoreDefinition(
        dataSourceLookup = "java:global/MyApp/MyDataSource",
        callerQuery = "select password from applicationuser where username = ?",
        // without this, it will always check for role groups, which fails
        // in this example
        useFor = IdentityStore.ValidationType.VALIDATE
)
@Singleton
@Startup
public class Config {
    @EJB
    UserService service;

    @Inject
    private Pbkdf2PasswordHash passwordHash;

    @PostConstruct
    public void init() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("Pbkdf2PasswordHash.Iterations", "3072");
        parameters.put("Pbkdf2PasswordHash.Algorithm", "PBKDF2WithHmacSHA512");
        parameters.put("Pbkdf2PasswordHash.SaltSizeBytes", "64");
        passwordHash.initialize(parameters);

        service.add(new ApplicationUser("bob", passwordHash.generate("builder".toCharArray())));
        service.add(new ApplicationUser("cat", passwordHash.generate("builder".toCharArray())));
    }
}
