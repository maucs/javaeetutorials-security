package auth;

import auth.credential.OAuthTokenCredential;
import auth.credential.OAuthUserCredential;
import org.omnifaces.util.Faces;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.CallerOnlyCredential;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStoreHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static javax.security.enterprise.identitystore.CredentialValidationResult.Status;

@AutoApplySession
@ApplicationScoped
public class BasicAuthenticationMechanism implements HttpAuthenticationMechanism {
    @Inject
    private IdentityStoreHandler identityStoreHandler;

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req, HttpServletResponse resp, HttpMessageContext ctx) throws AuthenticationException {
        if (ctx.isAuthenticationRequest()) {
            Credential cred = ctx.getAuthParameters().getCredential();

            if (cred instanceof CallerOnlyCredential)
                return validateSignUpCompleteCredential(ctx, (CallerOnlyCredential) cred);
            else if (cred instanceof OAuthTokenCredential)
                // credential is a custom OAuth with provider(Github) and access-token
                return validateOAuthCredential(ctx, (OAuthTokenCredential) cred);
        }
        return ctx.doNothing();
    }

    private AuthenticationStatus validateSignUpCompleteCredential(HttpMessageContext ctx, CallerOnlyCredential cred) {
        ctx.redirect("index.xhtml");
        return ctx.notifyContainerAboutLogin(cred.getCaller(), null);
    }

    /**
     * First, check for valid OAuth login. After that, check whether user is in database or not
     * <p>
     * Both features cannot combine to a single IdentityStore because there are three states to consider, invalid, valid but need signup, and valid
     * <p>
     * There are ways, by using either the unique caller ID, or identity store ID, to differentiate between registered users, minimizing code usage
     */
    private AuthenticationStatus validateOAuthCredential(HttpMessageContext ctx, OAuthTokenCredential cred) {
        final CredentialValidationResult result = identityStoreHandler.validate(cred);

        if (result.getStatus() != Status.VALID)
            return ctx.doNothing();

        OAuthUserCredential oAuthUser = new OAuthUserCredential(result.getCallerPrincipal().getName());
        CredentialValidationResult validate = identityStoreHandler.validate(oAuthUser);

        // do all redirection logic here instead of the login bean
        if (validate.getStatus() == Status.VALID) {
            ctx.redirect("index.xhtml");
            return ctx.notifyContainerAboutLogin(validate);
        } else {
            // send user to registration page
            // this is the easiest way to pass data without creating new beans
            Faces.getSession().setAttribute("signup_provider", oAuthUser.getProvider());
            Faces.getSession().setAttribute("signup_provider_id", oAuthUser.getId());
            return ctx.redirect("signup-oauth.xhtml");
        }
    }
}
