package auth.credential;

import javax.security.enterprise.credential.Credential;

/**
 * This credential only deals with tokens, not user login (yet)
 */
public class OAuthTokenCredential implements Credential {
    private String token, provider;

    public OAuthTokenCredential(String provider, String token) {
        this.token = token;
        this.provider = provider;
    }

    public String getToken() {
        return token;
    }

    public String getProvider() {
        return provider;
    }
}
