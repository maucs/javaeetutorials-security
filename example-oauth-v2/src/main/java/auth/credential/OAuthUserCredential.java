package auth.credential;

import javax.security.enterprise.credential.Credential;

/**
 * This class deals with logging user using token credentials
 * <p>
 * A helper static function is used to compress all available data into a CallerPrincipal. A hack, but no other solution exist
 */
public class OAuthUserCredential implements Credential {
    private String provider, id, name, fullname;

    public OAuthUserCredential(String token) {
        String[] split = token.split(":", 4);
        provider = split[0];
        id = split[1];
        name = split[2];
        fullname = split[3];
    }

    public static String compress(String provider, String id, String name, String fullname) {
        return String.format("%s:%s:%s:%s", provider, id, name, fullname);
    }

    public String getProvider() {
        return provider;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullname() {
        return fullname;
    }
}
