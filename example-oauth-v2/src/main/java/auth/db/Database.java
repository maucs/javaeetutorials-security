package auth.db;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This database stores OAuth provider details
 */
@Singleton
@Startup
public class Database {
    private Map<Long, Details> database = new HashMap<>();

    //    @PostConstruct
    // this is only to test whether the success page works or not
    public void init() {
        database.put(1L, new Details("foobarbaz", "GITHUB", "1415703"));
    }

    public Optional<String> get(String provider, String provider_id) {
        for (Details detail : database.values())
            if (detail.provider.equals(provider) && detail.provider_id.equals(provider_id))
                return Optional.of(detail.username);
        return Optional.empty();
    }

    public void add(String username, String provider, String provider_id) {
        database.put(database.size() + 1L, new Details(username, provider, provider_id));
    }

    private static class Details {
        public String username, provider, provider_id;

        public Details(String username, String provider, String provider_id) {
            this.username = username;
            this.provider = provider;
            this.provider_id = provider_id;
        }
    }
}
