package auth.store;

import auth.credential.OAuthTokenCredential;
import auth.credential.OAuthUserCredential;
import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.model.OAuth2AccessToken;
import config.GithubService;
import org.kohsuke.github.GHMyself;
import org.kohsuke.github.GitHub;

import javax.enterprise.context.RequestScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * This identity store's job is to check whether the token is valid
 */
@RequestScoped
public class OAuthTokenIdentityStore implements IdentityStore {
    @Override
    public CredentialValidationResult validate(Credential credential) {
        if (credential instanceof OAuthTokenCredential) {
            try {
                OAuth2AccessToken token = GithubService.service.getAccessToken(((OAuthTokenCredential) credential).getToken());
                final GHMyself github = GitHub.connect(null, token.getAccessToken()).getMyself();
                return new CredentialValidationResult(OAuthUserCredential.compress("GITHUB", String.valueOf(github.getId()), github.getLogin(), github.getName()));
            } catch (IOException | InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } catch (OAuthException e) {
                return CredentialValidationResult.INVALID_RESULT;
            }
        }
        return CredentialValidationResult.NOT_VALIDATED_RESULT;
    }

    @Override
    public Set<ValidationType> validationTypes() {
        return Collections.singleton(ValidationType.VALIDATE);
    }
}
