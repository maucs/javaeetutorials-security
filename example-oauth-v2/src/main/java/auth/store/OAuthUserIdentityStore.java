package auth.store;

import auth.credential.OAuthUserCredential;
import auth.db.Database;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Collections;
import java.util.Set;

import static javax.security.enterprise.identitystore.CredentialValidationResult.INVALID_RESULT;
import static javax.security.enterprise.identitystore.CredentialValidationResult.NOT_VALIDATED_RESULT;

/**
 * This identity store's job is to check whether the OAuth user is in the database
 */
@RequestScoped
public class OAuthUserIdentityStore implements IdentityStore {
    @EJB
    private Database database;

    @Override
    public CredentialValidationResult validate(Credential credential) {
        if (credential instanceof OAuthUserCredential) {
            String provider = ((OAuthUserCredential) credential).getProvider();
            String provider_id = ((OAuthUserCredential) credential).getId();
            return database.get(provider, provider_id)
                    .map(CredentialValidationResult::new)
                    .orElse(INVALID_RESULT);
        }
        return NOT_VALIDATED_RESULT;
    }

    @Override
    public Set<ValidationType> validationTypes() {
        return Collections.singleton(ValidationType.VALIDATE);
    }
}
