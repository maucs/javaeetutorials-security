package auth.store;

import auth.credential.OAuthTokenCredential;
import auth.db.Database;
import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.model.OAuth2AccessToken;
import config.GithubService;
import org.kohsuke.github.GHMyself;
import org.kohsuke.github.GitHub;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static javax.security.enterprise.identitystore.CredentialValidationResult.NOT_VALIDATED_RESULT;

/**
 * This identity store's job is to check whether the token is valid
 */
@ApplicationScoped
public class AlternativeOAuthTokenIdentityStore implements IdentityStore {
    @EJB
    private Database database;

    @Override
    public CredentialValidationResult validate(Credential credential) {
        if (credential instanceof OAuthTokenCredential) try {
            OAuth2AccessToken token = GithubService.service.getAccessToken(((OAuthTokenCredential) credential).getToken());
            final GHMyself github = GitHub.connect(null, token.getAccessToken()).getMyself();

            String provider = ((OAuthTokenCredential) credential).getProvider();
            String id = String.valueOf(github.getId());

            return database.get(provider, id)
                    .map(s -> new CredentialValidationResult(null, s, null, s, Collections.emptySet()))
                    .orElseGet(() -> {
                        ((OAuthTokenCredential) credential).setId(id);
                        return new CredentialValidationResult("null");
                    });
        } catch (IOException | InterruptedException | ExecutionException | OAuthException e) {
            return CredentialValidationResult.INVALID_RESULT;
        }
        return NOT_VALIDATED_RESULT;
    }

    @Override
    public Set<ValidationType> validationTypes() {
        return Collections.singleton(ValidationType.VALIDATE);
    }
}
