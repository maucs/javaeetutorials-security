package config;

import com.github.scribejava.apis.GitHubApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.OAuth20Service;

public class GithubService {
    // fill in the blanks
    private static final String SECRET = "SECRET",
            API_KEY = "",
            API_SECRET = "";

    public static final OAuth20Service service = new ServiceBuilder(API_KEY)
            .apiSecret(API_SECRET)
            .state(SECRET)
            .callback("http://localhost:8080/example-oauth/callback.xhtml")
            .build(GitHubApi.instance());

    public static String getSecret() {
        return SECRET;
    }
}
