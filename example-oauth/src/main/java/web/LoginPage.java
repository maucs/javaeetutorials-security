package web;

import auth.credential.OAuthTokenCredential;
import config.GithubService;
import org.omnifaces.util.Faces;

import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.Credential;
import javax.servlet.ServletException;
import java.io.IOException;

@Model
public class LoginPage {
    @Inject
    private SecurityContext securityContext;

    public void authenticateUser() {
        String url = GithubService.service.getAuthorizationUrl();
        Faces.redirectPermanent(url);
    }

    public void logout(ActionEvent event) throws ServletException {
        Faces.logout();
        Faces.invalidateSession();
    }

    // parameter accessed from JSF page
    public void login(String code) throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        Credential credential = new OAuthTokenCredential("GITHUB", code);

        AuthenticationStatus status = securityContext.authenticate(
                Faces.getRequest(),
                Faces.getResponse(),
                AuthenticationParameters.withParams().credential(credential));

        switch (status) {
            case SEND_CONTINUE:
            case SUCCESS:
                // Sign-up part pushed from AuthMechanism, no work is supposed to be done here
                context.responseComplete();
                return;
            default:
                context.getExternalContext().responseSendError(401, "");
                context.responseComplete();
        }
    }
}
