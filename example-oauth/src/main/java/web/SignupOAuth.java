package web;

import auth.db.Database;
import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.CallerOnlyCredential;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;

@Named
@ViewScoped
public class SignupOAuth implements Serializable {
    @EJB
    Database database;

    @Inject
    SecurityContext securityContext;

    private String provider, provider_id;

    @NotNull
    private String username;

    @PostConstruct
    public void init() {
        provider = (String) Faces.getSession().getAttribute("signup_provider");
        provider_id = (String) Faces.getSession().getAttribute("signup_provider_id");
    }

    // should check for duplicate username
    public void signup() throws IOException {
        database.add(username, provider, provider_id);
        securityContext.authenticate(Faces.getRequest(),
                Faces.getResponse(),
                AuthenticationParameters.withParams().credential(new CallerOnlyCredential(username)));
        Faces.responseComplete();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
