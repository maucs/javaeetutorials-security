package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.IdentityStore;
import javax.security.enterprise.identitystore.IdentityStoreHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@DatabaseIdentityStoreDefinition(
        dataSourceLookup = "java:global/MyApp/MyDataSource",
        callerQuery = "select password from userapplication where name = ?",
        useFor = IdentityStore.ValidationType.VALIDATE)
@AutoApplySession
@ApplicationScoped
public class BasicAuthenticationMechanism implements HttpAuthenticationMechanism {
    @Inject
    private IdentityStoreHandler identityStoreHandler;

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req, HttpServletResponse resp, HttpMessageContext ctx) throws AuthenticationException {
        // triggers when explicit auth is requested
        if (ctx.isAuthenticationRequest()) {
            Credential credential = ctx.getAuthParameters().getCredential();
            return ctx.notifyContainerAboutLogin(identityStoreHandler.validate(credential));
        }
        return ctx.doNothing();
    }
}
