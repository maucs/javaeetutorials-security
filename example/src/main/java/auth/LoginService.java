package auth;

import entity.UserApplication;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;

/**
 * The name should be UserLoginService, as it deals with the administration of user details
 */
@Stateless
public class LoginService {
    @EJB
    TokenService tokenService;

    @PersistenceContext
    EntityManager em;

    @Inject
    private Pbkdf2PasswordHash hash;

    // checks should always be done again
    public void addUser(String username, String password, String email, String baseUrl) {
        em.persist(new UserApplication(username, hash.generate(password.toCharArray()), email));
        tokenService.addToken("signup", username, baseUrl);
    }

    public void addUser(UserApplication user, String baseUrl) {
        em.persist(user);
        tokenService.addToken("signup", user.getName(), baseUrl);
    }

    public UserApplication getUserApplication(String user) {
        return em.createQuery("SELECT u FROM UserApplication u WHERE u.name = :name", UserApplication.class)
                .setParameter("name", user)
                .getSingleResult();
    }

    public boolean checkDuplicateUser(String user) {
        try {
            final UserApplication user1 = em.createQuery("SELECT u FROM UserApplication u WHERE u.name = :user", UserApplication.class)
                    .setParameter("user", user)
                    .getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    /**
     * Not supposed to be part of LoginService, but meh
     *
     * @param user
     */
    public void setUserUnlimited(String user) {
        getUserApplication(user).setLimitedAccess(false);
    }

    public void changePassword(String user, String password) {
        getUserApplication(user).setPassword(hash.generate(password.toCharArray()));
    }
}
