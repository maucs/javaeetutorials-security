package auth;

import javax.ejb.Asynchronous;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@Lock(LockType.READ)
public class TokenService {
    private Map<String, S> store = new ConcurrentHashMap<>();

    @Asynchronous
    public void addToken(String type, String user, String url) {
        final String uuid = UUID.randomUUID().toString();
        store.put(uuid, new S(user, type, Instant.now().plus(5, ChronoUnit.MINUTES)));
        System.out.println(url + "verify.xhtml?token=" + uuid);
    }

    /**
     * Can return null
     * <p>
     * Remove token upon successful check
     *
     * @param token
     * @return
     */
    public S checkToken(String token) {
        final S result = store.getOrDefault(token, null);
        store.remove(token);
        return result;
    }

    public static class S {
        public String user;
        public String type;
        public Instant time;

        public S(String user, String type, Instant time) {
            this.user = user;
            this.type = type;
            this.time = time;
        }
    }
}
