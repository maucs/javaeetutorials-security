package config;

import auth.LoginService;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.inject.Named;
import javax.inject.Singleton;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau")
@Named
@Singleton
@Startup
public class Database {
    @EJB
    private LoginService loginService;

    @PostConstruct
    public void init() {
        // doesn't work somehow
        // loginService.addUser("bob", "dsfsdfsdfsdfsdf", "f@f.com");
    }
}