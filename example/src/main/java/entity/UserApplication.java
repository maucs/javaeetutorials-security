package entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

// should i move the validations here?
@Entity
public class UserApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    // first character must be letter, followed by alnum+_, ends with alnum
    @Pattern(regexp = "^[\\p{Alpha}][\\p{Alnum}_]+[\\p{Alnum}]")
    private String name;

    @NotNull
    @Size(min = 6)
    private String password;

    @NotNull
    private String email;
    private Boolean limitedAccess;

    public UserApplication() {
    }

    public UserApplication(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
        limitedAccess = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getLimitedAccess() {
        return limitedAccess;
    }

    public void setLimitedAccess(Boolean limitedAccess) {
        this.limitedAccess = limitedAccess;
    }
}
