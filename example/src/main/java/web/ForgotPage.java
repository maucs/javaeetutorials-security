package web;

import auth.LoginService;
import auth.TokenService;
import org.omnifaces.util.Faces;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class ForgotPage {
    @EJB
    TokenService tokenService;

    @EJB
    LoginService loginService;

    private String username;

    // log token instead
    public void submit() {
        if (loginService.checkDuplicateUser(username))
            tokenService.addToken("forgot", username, Faces.getRequestBaseURL());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
