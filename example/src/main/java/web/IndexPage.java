package web;

import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.ServletException;

@Named
@RequestScoped
public class IndexPage {
    public void logout() throws ServletException {
        Faces.invalidateSession();
        Faces.logout();
    }
}
