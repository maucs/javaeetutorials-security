package web;

import auth.LoginService;
import org.omnifaces.util.Faces;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.UsernamePasswordCredential;

@Named
@RequestScoped
public class LoginPage {
    @EJB
    LoginService loginService;

    @Inject
    UserSession session;

    @Inject
    SecurityContext securityContext;

    private String username, password;

    public String login() {
        switch (securityContext.authenticate(Faces.getRequest(), Faces.getResponse(),
                AuthenticationParameters.withParams().credential(new UsernamePasswordCredential(username, password)))) {
            case SUCCESS:
                session.setUser(loginService.getUserApplication(username));
                return "index?faces-redirect=true";
            default:
                Faces.getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Username or Password", null));
                return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
