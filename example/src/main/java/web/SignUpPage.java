package web;

import auth.LoginService;
import entity.UserApplication;
import org.omnifaces.util.Faces;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Named
@RequestScoped
public class SignUpPage {
    @EJB
    private LoginService loginService;

    private UserApplication user = new UserApplication();

    public String action() {
//        loginService.addUser(username, password, email, Faces.getRequestBaseURL());
        loginService.addUser(user, Faces.getRequestBaseURL());
        return "success?faces-redirect=true";
    }

    public void validateDuplicateUser(FacesContext context,
                                      UIComponent toValidate,
                                      Object value) {
        // if duplicate name found
        if (loginService.checkDuplicateUser((String) value)) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage("User exist, please choose a different username");
            context.addMessage(toValidate.getClientId(context), message);
        }
    }

    // Java EE 8 has @Email annotation
    public void validateEmail(FacesContext context,
                              UIComponent toValidate,
                              Object value) {
        try {
            InternetAddress email = new InternetAddress((String) value);
            email.validate();
        } catch (AddressException ex) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage("Invalid Email");
            context.addMessage(toValidate.getClientId(context), message);
        }
    }

    public UserApplication getUser() {
        return user;
    }

    public void setUser(UserApplication userApplication) {
        this.user = userApplication;
    }
}
