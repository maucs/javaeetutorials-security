package web;

import entity.UserApplication;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Problems: when user has updated data, it will not change here
 * <p>
 * In this case, if user verifies themselves after they log in, user will still be unverified even though they are not
 */
@Named
@SessionScoped
public class UserSession implements Serializable {
    private UserApplication user;

    public UserApplication getUser() {
        return user;
    }

    public void setUser(UserApplication user) {
        this.user = user;
    }
}
