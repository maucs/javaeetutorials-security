package web;

import auth.LoginService;
import auth.TokenService;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

/**
 * This whole class is built upon hacks
 */
@Named
@ViewScoped
public class VerifyPage implements Serializable {
    @EJB
    LoginService loginService;

    @EJB
    TokenService tokenService;

    private String name, type, token, password;
    private boolean changePasswordSuccess;

    public void action() {
        System.out.println("running action");
        if (token == null)
            return;

        Optional.ofNullable(tokenService.checkToken(token))
                .ifPresent(s -> {
                    type = s.type;
                    switch (type) {
                        case "signup":
                            loginService.setUserUnlimited(s.user);
                            return;
                        case "forgot":
                            name = loginService.getUserApplication(s.user).getName();
                            return;
                    }
                });
    }

    public void submitPasswordChange() {
        type = "forgot";
        loginService.changePassword(name, password);
        changePasswordSuccess = true;
        System.out.println("Changed");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isChangePasswordSuccess() {
        return changePasswordSuccess;
    }

    public void setChangePasswordSuccess(boolean changePasswordSuccess) {
        this.changePasswordSuccess = changePasswordSuccess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
