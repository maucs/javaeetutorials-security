package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Collections;

@ApplicationScoped
public class BasicIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bobbob", "thebuilder"))
            return new CredentialValidationResult("bobbob", Collections.singleton("foo"));
        return CredentialValidationResult.INVALID_RESULT;
    }
}
