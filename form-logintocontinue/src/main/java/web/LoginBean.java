package web;

import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.ServletException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;

import static javax.security.enterprise.AuthenticationStatus.SUCCESS;

@Named
@RequestScoped
public class LoginBean {
    @Inject
    private SecurityContext securityContext;

    @NotNull
    @Size(min = 3, max = 10, message = "Username length must be between 3 and 5 characters")
    private String username;

    @NotNull
    @Size(min = 5, max = 50, message = "Password length must be between 5 and 50 characters")
    private String password;

    private String forwardUrl;

    @PostConstruct
    public void init() {
        forwardUrl = (String) Faces.getRequest().getAttribute("javax.servlet.forward.request_uri");
    }

    public void login() throws IOException {
        // prevent session fixation, a trade-off cause old data is gone
        Faces.invalidateSession();

        FacesContext context = FacesContext.getCurrentInstance();
        Credential credential = new UsernamePasswordCredential(username, new Password(password));

        AuthenticationStatus status = securityContext.authenticate(
                Faces.getRequest(),
                Faces.getResponse(),
                AuthenticationParameters.withParams().credential(credential));

        if (status.equals(SUCCESS)) {
            if (forwardUrl != null) {
                context.getExternalContext().redirect(forwardUrl);
            }
            context.getExternalContext().redirect("/index.xhtml");
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failure", null));
        }
    }

    public void logout() throws ServletException {
        Faces.logout();
        Faces.invalidateSession();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }
}
