package service;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

@Stateless
public class ProtectedService {
    @RolesAllowed("foo")
    public void empty() {
    }
}
