package web;

import service.ProtectedService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Named
@RequestScoped
public class Bean {
    @EJB
    ProtectedService service;

    ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
    Principal principal = ctx.getUserPrincipal();

    public Principal getPrincipal() {
        return principal;
    }

    public String logout() throws ServletException {
        ((HttpServletRequest) ctx.getRequest()).logout();
        ctx.invalidateSession();
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + "?faces-redirect=true";
    }

    public void protectedService() {
        service.empty();
    }
}
