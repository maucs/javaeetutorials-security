package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Collections;

@BasicAuthenticationMechanismDefinition(
        realmName = "test realm"
)
@ApplicationScoped
public class BasicIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bob", "builder"))
            return new CredentialValidationResult("bob", Collections.singleton("user"));
        return CredentialValidationResult.INVALID_RESULT;
    }
}