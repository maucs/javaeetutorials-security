package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Collections;

/**
 * If passed from here, user has valid token, otherwise check for normal access
 */
@ApplicationScoped
public class TokenIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bob", "token123"))
            return new CredentialValidationResult("bob", Collections.singleton("token"));
        return CredentialValidationResult.INVALID_RESULT;
    }

    @Override
    public int priority() {
        return 90;
    }
}