package provider;

import javax.ejb.EJBAccessException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EJBAccessExceptionMapper implements ExceptionMapper<EJBAccessException> {
    @Override
    public Response toResponse(EJBAccessException exception) {
        JsonObject object = Json
                .createObjectBuilder()
                .add("error", "Access Denied")
                .build();
        return Response
                .status(Response.Status.UNAUTHORIZED)
                .entity(object)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
