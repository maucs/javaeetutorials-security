package service;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

// curl -v http://localhost:8080/jaxrs-basic/api/secure/public
// curl -v -u bob:builder http://localhost:8080/jaxrs-basic/api/secure/normal
// curl -v -u bob:token123 http://localhost:8080/jaxrs-basic/api/secure/token
@BasicAuthenticationMechanismDefinition
@Path("secure")
@Stateless
@RolesAllowed("user")
public class HelloWorldService {
    @Context
    SecurityContext context;

    @GET
    @Path("public")
    @PermitAll
    public String getPublic() {
        return "public access";
    }

    @GET
    @Path("normal")
    public String getNormal() {
        return "normal protected access";
    }

    @GET
    @Path("token")
    @RolesAllowed("token")
    public String getToken() {
        return "token access";
    }
}
