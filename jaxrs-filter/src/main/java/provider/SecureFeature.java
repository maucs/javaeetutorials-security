package provider;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Stateless
@Provider
public class SecureFeature implements DynamicFeature {
    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @Inject
    SecurityContext securityContext;

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        SECURE secures = resourceInfo.getResourceMethod().getAnnotation(SECURE.class);
        if (secures == null)
            return;
        context.register(new SecureFilter());
    }

    private class SecureFilter implements ContainerResponseFilter {
        @Override
        public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            UsernamePasswordCredential credential = new UsernamePasswordCredential(username, password);

            AuthenticationStatus status = securityContext.authenticate(
                    request, response, AuthenticationParameters.withParams().credential(credential)
            );

            // if it's not a success
            if (!status.equals(AuthenticationStatus.SUCCESS)) {
                responseContext.setStatus(401);
                responseContext.setEntity("not allowed", null, MediaType.APPLICATION_JSON_TYPE);
            }
        }
    }
}
