package service;

import provider.SECURE;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("service")
public class Service {
    // curl -v "localhost:8080/jaxrs-filter/api/service/secure?username=bobbob&password="
    // curl -v "localhost:8080/jaxrs-filter/api/service/secure?username=bobbob&password=thebuilder"
    // notice the return code
    @GET
    @SECURE
    @Path("secure")
    public String secure() {
        return "secure";
    }

    // curl -v "localhost:8080/jaxrs-filter/api/service/public"
    @GET
    @Path("public")
    public String everybody() {
        return "public";
    }
}
