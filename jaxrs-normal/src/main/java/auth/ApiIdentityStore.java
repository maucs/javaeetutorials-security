package auth;

import credential.ApiCredential;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Collections;

@ApplicationScoped
public class ApiIdentityStore implements IdentityStore {
    @Override
    public CredentialValidationResult validate(Credential credential) {
        System.out.println("IdentityStore: " + getClass().getName());
        if (credential instanceof ApiCredential)
            if (((ApiCredential) credential).compareTo("bob", "thebuilder"))
                return new CredentialValidationResult("bob", Collections.singleton("foo"));
        return CredentialValidationResult.INVALID_RESULT;
    }

    @Override
    public int priority() {
        return 99;
    }
}
