package credential;

import javax.security.enterprise.credential.Credential;

/**
 * Extremely naive implementation. Refer to {@link javax.security.enterprise.credential.UsernamePasswordCredential} for proper implementation
 * <p>
 * This is only so that IdentityStore can make specific call
 */
public class ApiCredential implements Credential {
    private final String username, password;

    public ApiCredential(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean compareTo(String u, String p) {
        return u.equals(username) && p.equals(password);
    }
}
