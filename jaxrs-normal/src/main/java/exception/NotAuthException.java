package exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true) // for fun, does nothing in this example
public class NotAuthException extends Exception {
    private String reason;

    public NotAuthException(String message) {
        reason = message;
    }

    public String getReason() {
        return reason;
    }
}
