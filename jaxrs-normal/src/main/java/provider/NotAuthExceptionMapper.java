package provider;

import exception.NotAuthException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotAuthExceptionMapper implements ExceptionMapper<NotAuthException> {
    @Override
    public Response toResponse(NotAuthException exception) {
        JsonObject object = Json
                .createObjectBuilder()
                .add("error", exception.getReason())
                .build();
        return Response
                .status(Response.Status.UNAUTHORIZED)
                .entity(object)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
