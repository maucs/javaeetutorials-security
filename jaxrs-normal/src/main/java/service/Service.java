package service;

import credential.ApiCredential;
import exception.NotAuthException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.Credential;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;

@Stateless
@Path("service")
public class Service {
    @Inject
    SecurityContext securityContext;

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @Context
    private HttpHeaders headers;

    // curl -u bob:thebuilder -v localhost:8080/jaxrs-normal/api/service/response
    // curl -v localhost:8080/jaxrs-normal/api/service/response
    @GET
    @Path("response")
    public Response response() throws NotAuthException {
        if (!getAuthStatus().equals(AuthenticationStatus.SUCCESS)) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity("{\"error\": \"incorrect user or password\"}")
                    .build();

        }
        return Response.ok("secure response").build();
    }

    // curl -u bob:thebuilder -v localhost:8080/jaxrs-normal/api/service
    @GET
    public String secure() throws NotAuthException {
        if (!getAuthStatus().equals(AuthenticationStatus.SUCCESS))
            throw new NotAuthException("incorrect user or password");
        return "secure";
    }

    // curl --digest -u bob:thebuilder -v localhost:8080/jaxrs-normal/api/service
    // curl -v localhost:8080/jaxrs-normal/api/service
    public Credential getCredential() throws NotAuthException {
        String auth = headers.getHeaderString("Authorization");
        if (auth != null && auth.startsWith("Basic ")) {
            String[] s = new String(parseBase64Binary(auth.substring(6))).split(":");
            // if this is used, ApiIdentityStore will be called. check log
            // return new UsernamePasswordCredential(s[0], s[1]);
            return new ApiCredential(s[0], s[1]);
        }
        throw new NotAuthException("please supply a proper auth");
    }

    public AuthenticationStatus getAuthStatus() throws NotAuthException {
        Credential credential = getCredential();
        return securityContext.authenticate(request, response,
                AuthenticationParameters.withParams().credential(credential));
    }
}
