package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

@ApplicationScoped
public class BasicIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bob", "thebuilder")) {
            System.out.println("Logged in from " + this.getClass().getName());
            return new CredentialValidationResult("bob");
        }
        System.out.println("Rejected from " + this.getClass().getName());
        return CredentialValidationResult.INVALID_RESULT;
    }
}
