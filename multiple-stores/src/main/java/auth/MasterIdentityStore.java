package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

@ApplicationScoped
public class MasterIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bob", "master")) {
            System.out.println("Logged in from " + this.getClass().getName());
            // you can return the store name here, if it's important
            // new CredentialValidationResult("store_name", "caller_name", "ldap_name", "uniqueid", Collections.singleton("admin"));
            return new CredentialValidationResult("bob");
        }
        System.out.println("Rejected from " + this.getClass().getName());
        return CredentialValidationResult.INVALID_RESULT;
    }

    @Override
    public int priority() {
        return 1;
    }
}
