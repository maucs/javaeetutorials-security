package auth;

import principal.UserAgePrincipal;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStoreHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static javax.security.enterprise.identitystore.CredentialValidationResult.Status.VALID;

@AutoApplySession
@ApplicationScoped
public class BasicAuthenticationMechanism implements HttpAuthenticationMechanism {
    @Inject
    private IdentityStoreHandler identityStoreHandler;

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req, HttpServletResponse resp, HttpMessageContext ctx) throws AuthenticationException {
        // triggers when explicit auth is requested
        if (ctx.isAuthenticationRequest()) {
            Credential credential = ctx.getAuthParameters().getCredential();
            CredentialValidationResult result = identityStoreHandler.validate(credential);
            if (result.getStatus().equals(VALID))
                return ctx.notifyContainerAboutLogin(new UserAgePrincipal(result.getCallerPrincipal().getName(), 20), null);
            else
                return AuthenticationStatus.SEND_FAILURE;
        }

        if (ctx.isProtected())
            return ctx.responseUnauthorized();

        return ctx.doNothing();
    }
}
