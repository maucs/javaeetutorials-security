package principal;

import java.security.Principal;

/**
 * This is only an example
 */
public class UserAgePrincipal implements Principal {
    private String name;
    private Integer age;

    public UserAgePrincipal(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}
