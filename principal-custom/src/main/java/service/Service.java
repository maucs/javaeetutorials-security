package service;

import principal.UserAgePrincipal;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import java.security.Principal;

@Stateless
public class Service {
    @Inject
    SecurityContext context;

    public void check() {
        Principal principal = context.getCallerPrincipal();

        if (principal == null)
            return;

        System.out.println("Principal: " + principal.getName());

        if (principal instanceof UserAgePrincipal) {
            UserAgePrincipal p = (UserAgePrincipal) principal;
            System.out.println("UserAgePrincipal: " + p.getName() + ", " + p.getAge());
        }
    }
}
