package web;

import org.omnifaces.util.Faces;
import principal.NotUsedPrincipal;
import principal.UserAgePrincipal;
import service.Service;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;
import java.security.Principal;
import java.util.Iterator;

@Named
@RequestScoped
public class MainPage {
    String name;
    Integer age;

    @EJB
    Service service;

    @Inject
    SecurityContext securityContext;

    FacesContext context = FacesContext.getCurrentInstance();

    // the few ways of doing it
    // soteria examples turns the set into an array
    // test for EJB
    public void get() {
        service.check();

        Iterator<UserAgePrincipal> i = securityContext.getPrincipalsByType(UserAgePrincipal.class).iterator();
        if (i.hasNext()) {
            UserAgePrincipal principal = i.next();
            name = principal.getName();
            age = principal.getAge();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You have not logged in yet"));
        }

        Principal userPrincipal = Faces.getRequest().getUserPrincipal();
        if (userPrincipal instanceof UserAgePrincipal) {
            UserAgePrincipal p = (UserAgePrincipal) userPrincipal;
            context.addMessage(null, new FacesMessage("UserAgePrincipal, " + p.getName() + ", " + p.getAge()));
        }

        if (userPrincipal instanceof NotUsedPrincipal) {
            NotUsedPrincipal p = (NotUsedPrincipal) userPrincipal;
            context.addMessage(null, new FacesMessage("UserAgePrincipal, " + p.getName()));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
