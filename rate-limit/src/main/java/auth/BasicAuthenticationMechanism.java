package auth;

import ratelimit.ReachLimit;
import ratelimit.Store;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStoreHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Singleton
@ApplicationScoped
public class BasicAuthenticationMechanism implements HttpAuthenticationMechanism {
    @Inject
    private IdentityStoreHandler identityStoreHandler;

    @Inject
    private ReachLimit limit;

    @EJB
    private Store store;

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req, HttpServletResponse resp, HttpMessageContext ctx) throws AuthenticationException {
        String user = req.getParameter("user");
        String password_req = req.getParameter("password");

        if (user != null || password_req != null) {
            Password password = new Password(password_req);
            CredentialValidationResult result = identityStoreHandler.validate(new UsernamePasswordCredential(user, password));

            if (result.getIdentityStoreId() != null &&
                    result.getIdentityStoreId().equals(RateLimitIdentityStore.class.getName())) {
                // return a too much thing
                limit.setReachLimit(true);
                return ctx.doNothing();
            }

            // do not pass CredentialValidationResult, error
            // pass only the minimum parameters required
            store.actOnUser(user, result.getStatus());

            if (result.getStatus() == CredentialValidationResult.Status.VALID)
                return ctx.notifyContainerAboutLogin(result);
        }
        return ctx.doNothing();
    }
}
