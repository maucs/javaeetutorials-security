package auth;

import ratelimit.Store;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

/**
 * This identity store should only check whether further stores can validate login
 * The act of incrementing failed login or clearing should be done by them
 * <p>
 * OR PUSH IT TO AUTH MECHANISM
 * <p>
 * If limit is reach, returns a credential
 * <p>
 * If still good, returns invalid result
 */
@ApplicationScoped
public class RateLimitIdentityStore implements IdentityStore {
    @EJB
    private Store store;

    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (store.canLogIn(credential.getCaller()))
            return CredentialValidationResult.INVALID_RESULT;
        // "validate" and stop checking subsequent stores
        // callerName must not be empty
        return new CredentialValidationResult(getClass().getName(), credential.getCaller(), null, null, null);
    }

    /**
     * Always check rate-limit before validating
     */
    @Override
    public int priority() {
        return 1;
    }
}
