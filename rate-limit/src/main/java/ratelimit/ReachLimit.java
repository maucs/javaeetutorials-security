package ratelimit;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class ReachLimit {
    private boolean reachLimit = false;

    public boolean isReachLimit() {
        return reachLimit;
    }

    public void setReachLimit(boolean reachLimit) {
        this.reachLimit = reachLimit;
    }
}
