package ratelimit;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static javax.security.enterprise.identitystore.CredentialValidationResult.Status;
import static javax.security.enterprise.identitystore.CredentialValidationResult.Status.VALID;

@Singleton
@Lock(LockType.READ)
public class Store {
    private final Map<String, LastLogin> store = new ConcurrentHashMap<>();

    public boolean canLogIn(String name) {
        final LastLogin login = store.get(name);
        return login == null || login.canLogIn();
    }

    // with Redis's EXPIRE, there's no need to write all these code
    public void actOnUser(String user, Status status) {
        store.putIfAbsent(user, new LastLogin());
        if (status == VALID) {
            store.remove(user);
            return;
        }
        // don't clear it, addFailedAttempt assumes the user is still in the store
        if (Instant.now().isAfter(store.get(user).time.plus(2, ChronoUnit.MINUTES))) {
            store.put(user, new LastLogin());
        }
        addFailedAttempt(user);
    }

    private void addFailedAttempt(String name) {
        store.get(name).failLogIn();
    }

    private static class LastLogin {
        Integer count = 0;
        Instant time = Instant.now();

        boolean canLogIn() {
            // 5 failed login attempt, and next login is before now
            return count < 5 || time.isBefore(Instant.now());
        }

        void failLogIn() {
            count++;
            // adds 30s cooldown per login if failrate is above 5
            time = Instant.now().plus((count >= 5) ? 30 : 0, ChronoUnit.SECONDS);
        }
    }
}
