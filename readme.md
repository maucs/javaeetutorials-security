# Java EE Security

The latest Payara 4 (Build 174) targeting Java EE 7 adds support for Java EE Security 1.0

## Tutorial Ordering

Follows the order listed in pom.xml's modules

1. **url-parameter-auth**

   Failed authentication returns 401. If you don't want that, remove `ctx.responseUnauthorized();` from **BasicAuthenticationMechanism**
   
   Even if annotating scope seems redundant, it has an effect on the **remember-me** module
   
2. **auto-apply-session**

   On the **remember-me** example, the normal login does not remember. On going back to the main page, the user is logged out. **@AutoApplySession** sets it in the session, so the auth-check is done only once
 
3. **form-basic**

   Uses **@BasicAuthenticationMechanismDefinition** as the **HttpAuthenticationMechanism**, just implement **IdentityStore**(s)
   
   Extremely simple, there is no way to logout
   
   The three form example uses these built-in auth mechanisms, it has **@AutoApplySession**, but no remember-me feature. As such, they are only used for the simplest case

4. **form-normal**

   Uses **@FormAuthenticationMechanismDefinition** as the **HttpAuthenticationMechanism**, combine with web.xml security and servlet securities
   
5. **form-custom**

   **@CustomFormAuthenticationMechanismDefinition** for JSF forms, and **SecurityContext** to authenticate
   
6. **form-logintocontinue**

   Using **@LoginToContinue** to do redirect when accessing protected resources.   
   
7. **totally-custom**

   Using **HttpAuthenticationMechanism** and **@LoginToContinue**, combining them to create custom login flow
   
8. **totally-custom-auth**   

   A totally unique login flow using **SecurityContext**

   The difference with the previous module is that the login page is invoked by the user manually, not getting redirected. Going to a protected page shows an empty page, the code that returns 401 is in the auth mechanism
   
   The login method returns to a certain page. It can be controlled by getting _getRequestURL()_, and storing them either in the form as a hidden input, or as part of URL parameter (not safe)
   
9. **principal-custom**
   
   Using custom principals, if you do not return a fail in the auth mechanism, it will succeed. Return **SEND_FAILURE** so that it won't send a code 401 to the user
   
10. **multiple-stores**

    Using request variable in EL rather than through injecting managed bean
   
11. **jaxrs-normal,filter**
   
    **filter** use annotation to protect resources
    
    **normal** uses normal method, Basic Authentication, the example uses ExceptionMapper and custom credentials to differentiate login types
    
12. **db-xxx**

    **simple** leverages **@DatabaseIdentityStoreDefinition**
     
    **custom** creates its own **HttpAuthenticationMechanism**, and generates, validates the passwords manually. Also implements a custom **PasswordHash**
    
    **basic** uses EJB to deal with authentication and authorization. If all you need is a simple API call using these, then it's good enough
    
13. **rate-limit**

    Try fail logging till rate-limit error appears, then try logging in properly. You will not log in till the rate-limit error dissapears    
    
14. **example**

    That was extremely complicated
    
15. **example-oauth(-*)**

    Covering OAuth login, there are two examples, one using two IdentityStore and two types of Tokens to login, and another using only one Store and one Token, taking advantage of unique ID to login
    
    All redirection is specified in the AuthenticationMechanism. Web beans only need to call **ctx.responseComplete()**.
    
    Credentials can be used to store results of Identity Store, no need for (de)serialization of data like provider-uid
   
    v2 uses two tokens and two identity stores to verify proper token and is-user-in-database