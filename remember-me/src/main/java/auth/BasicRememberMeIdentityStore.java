package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.CallerPrincipal;
import javax.security.enterprise.credential.RememberMeCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.RememberMeIdentityStore;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationScoped
public class BasicRememberMeIdentityStore implements RememberMeIdentityStore {
    /**
     * A poor man's database
     */
    private final Map<String, CredentialValidationResult> identities = new ConcurrentHashMap<>();

    /**
     * Called on the first successful authentication
     */
    @Override
    public String generateLoginToken(CallerPrincipal callerPrincipal, Set<String> groups) {
        String token = UUID.randomUUID().toString();
        // the CredentialValidationResult is for retrieval later on
        // the token should be hashed server-side
        identities.put(token, new CredentialValidationResult(callerPrincipal, groups));
        return token;
    }

    /**
     * Called on every the first re-auth
     * <p>
     * Make sure token expiry is refreshed on the database-side
     */
    @Override
    public CredentialValidationResult validate(RememberMeCredential credential) {
        if (identities.containsKey(credential.getToken()))
            return identities.get(credential.getToken());
        return CredentialValidationResult.INVALID_RESULT;
    }

    @Override
    public void removeLoginToken(String token) {
        identities.remove(token);
    }
}
