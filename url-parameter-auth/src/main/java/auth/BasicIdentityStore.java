package auth;

import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

public class BasicIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential credential) {
        if (credential.compareTo("bob", "thebuilder"))
            return new CredentialValidationResult("bob");
        return CredentialValidationResult.INVALID_RESULT;
    }
}
