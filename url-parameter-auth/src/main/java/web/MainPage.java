package web;

import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.security.Principal;

@Named
@RequestScoped
public class MainPage {
    Principal userPrincipal;

    @PostConstruct
    public void init() {
        userPrincipal = Faces.getRequest().getUserPrincipal();
    }

    public Principal getUserPrincipal() {
        return userPrincipal;
    }
}
